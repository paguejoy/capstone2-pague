// window.location.search returns the query string part of the URL
console.log(window.location.search)

// instantiate a URLSearchParams object so we can execute methods to access 
// specific parts of the query string
let params = new URLSearchParams(window.location.search);

// spread the values to sey the key-value pairs of the object URLSearchParams
// console.log(...courseId);

// the has method checks if the courseId key exists in the URL query string
// true means that the key exists
console.log(params.has('courseId'))

// get method returns the value of the key passed in as an argument
// console.log(params.get('courseId'))

let courseId = params.get('courseId')
console.log(courseId);

let name = document.querySelector("#courseName")
let price = document.querySelector("#coursePrice")
let description = document.querySelector("#courseDescription")

fetch(`https://radiant-stream-95158.herokuapp.com/api/courses/${courseId}`)
.then(res => res.json())
.then(data => {

	console.log(data)

	// assign the current values as placeholders
	name.placeholder = data.name
	price.placeholder = data.price
	description.placeholder = data.description
	name.value = data.name
	price.value = data.price
	description.value = data.description

})

document.querySelector("#editCourse").addEventListener("submit", (e) => {

	e.preventDefault()

	let courseName;
	let desc;
	let priceValue;

    // if((name.value === '') && (price.value === '') && (description.value === '')) {

    //     courseName = data.name;
    //     priceVal = data.price;
    //     desc = data.description

    //     } else if(name.value === '') {

    //         priceVal = data.price;
    //         courseName = data.value;
    //         desc = description.value;

    //     } else if(price.value === '') {

    //         priceVal = data.price;
    //         courseName = name.value;
    //         desc = description.value;

    //     } else if(description.value === '') {

    //         desc = date.description;
    //         courseName = name.value;
    //         priceVal = price.value;

    // } else {

    //     courseName = name.value;
    //     priceVal = price.value;
    //     desc = description.value;
    // }

    courseName= name.value
    desc = description.value
    priceValue = price.value

	let token = localStorage.getItem('token')

	fetch('https://radiant-stream-95158.herokuapp.com/api/courses', {
        method: 'PUT',
        headers: {
            'Content-Type': 'application/json',
            'Authorization': `Bearer ${token}`
        },
        body: JSON.stringify({
            id: courseId,
            name: courseName,
            description: desc,
            price: priceValue
        })
    })
    .then(res => res.json())
    .then(data => {
        
    	console.log(data)

    	//creation of new course successful
    	if(data === true){
    	    //redirect to courses index page
    	    window.location.replace("./courses.html")
    	}else{
    	    //error in creating course, redirect to error page
    	    alert("something went wrong")
    	}

    })

})

/*let token = localStorage.getItem()

let name = document.querySelector('#name')
let email = document.querySelector('#email')
let mobileNo = document.querySelector('#mobileNo')

fetch(, {
    headers: {
        'Authorization': `Bearer ${token}`
    }
})
.then(res => res.json())
.then(data => {

    console.log(data);

    if(data){
        name.innerHTML = data.name
    }

})*/